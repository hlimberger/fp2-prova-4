package utfpr.ct.dainf.pratica;

/**
 * Linguagem Java
 * @author
 */

import java.util.Comparator;

public class LancamentoComparator implements Comparator<Lancamento> {
    
    public LancamentoComparator(){
        
    }
    
    @Override
    public int compare(Lancamento a, Lancamento b){
        
        int retorna = a.getConta() - b.getConta();
        
        if(retorna == 0){
            retorna = a.getData().compareTo(b.getData());
        }
        
        return retorna;
    }
    
}
